package org.kamaz;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple implementation of template store
 *
 */
public class TemplateStore implements ITemplateStore {

    private Map<String, DetailEntity> map = new HashMap<>();
    
    public void addValue(DetailEntity detailEntity){
        map.put(detailEntity.getIdentifier(), detailEntity);
    }
    
    @Override
    public DetailEntity getTemplateByKey(String identifier) {
        return map.get(identifier);
    }

    @Override
    public Iterable<DetailEntity> iterator() {
        return map.values();
    }   
    
}
