package org.kamaz.ui;

import org.eclipse.jface.wizard.Wizard;
import org.kamaz.ITemplateStore;
import org.kamaz.RecognitionBuilderResult;
import org.kamaz.ui.pages.CameraViewPage;
import org.kamaz.ui.pages.RecognitionResultPage;
import org.kamaz.ui.pages.ResultPage;

/**
 * 
 */
public class MainWizard extends Wizard {

    protected RecognitionBuilderResult recognitionBuilderResult;
    protected ITemplateStore templateStore;

    public MainWizard(RecognitionBuilderResult recognitionBuilderResult, ITemplateStore templateStore) {
        super();
        setNeedsProgressMonitor(true);
        this.recognitionBuilderResult = recognitionBuilderResult;
        this.templateStore = templateStore;
    }

    @Override
    public String getWindowTitle() {
        return "Detail recognition";
    }

    @Override
    public void addPages() {        
        CommonWizardPage one = new CameraViewPage(recognitionBuilderResult);
        CommonWizardPage two = new RecognitionResultPage(recognitionBuilderResult, templateStore);
        CommonWizardPage three = new ResultPage(recognitionBuilderResult);
        
        addPage(one);
        addPage(two);
        addPage(three);
    }

    @Override
    public boolean performFinish() {
        return true;
    }
}