package org.kamaz.ui;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.kamaz.RecognitionBuilderResult;

public class RecognitionWizardDialog extends WizardDialog {

    private RecognitionBuilderResult recognitionBuilderResult;

    public RecognitionWizardDialog(RecognitionBuilderResult recognitionBuilderResult, Shell parentShell,
            IWizard newWizard) {
        super(parentShell, newWizard);
        this.recognitionBuilderResult = recognitionBuilderResult;
    }

    @Override
    public void showPage(IWizardPage page) {
        ((CommonWizardPage) page).beforeLoad();

        super.showPage(page);
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        super.createButtonsForButtonBar(parent);

        Button virtualLibraButton = createButton(parent, IDialogConstants.CLIENT_ID, "����������� ����", false);
        virtualLibraButton.addListener(SWT.Selection, new Listener() {
            public void handleEvent(Event e) {
                if (SWT.Selection == e.type) {
                    LibraDialog libraDialog = new LibraDialog(parent.getShell());
                    if (libraDialog.open()) {
                        recognitionBuilderResult.setCommonWeight(libraDialog.getCommonWeight());
                        recognitionBuilderResult.setLibraWeight(libraDialog.getLibraWeight());
                    }
                }
            }
        });

    }

}
