package org.kamaz.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class LibraDialog extends Dialog {

    private String libraWeight;
    private String commonWeight;
    private boolean isCloseNormal;

    /**
     * InputDialog constructor
     * 
     * @param parent
     *            the parent
     */
    public LibraDialog(Shell parent) {
        // Pass the default styles here
        this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
    }

    /**
     * InputDialog constructor
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public LibraDialog(Shell parent, int style) {
        // Let users override the default styles
        super(parent, style);
        setText("Libra Dialog");
    }

    public int getLibraWeight() {
        return Integer.parseInt(libraWeight);
    }

    public int getCommonWeight() {
        return Integer.parseInt(commonWeight);
    }

    /**
     * Opens the dialog and returns the input
     * 
     * @return String
     */

    public boolean open() {
        // Create the dialog window
        Shell shell = new Shell(getParent(), getStyle());
        shell.setText(getText());
        createContents(shell);
        shell.pack();
        shell.open();
        Display display = getParent().getDisplay();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        return isCloseNormal;
    }

    /**
     * Creates the dialog's contents
     * 
     * @param shell
     *            the dialog window
     */
    private void createContents(final Shell shell) {
        shell.setLayout(new GridLayout(2, true));

        // Show the message
        Label label = new Label(shell, SWT.NONE);
        label.setText("������� ��� ����");
        GridData data = new GridData();
        data.horizontalSpan = 2;
        label.setLayoutData(data);

        // Display the input box
        final Text text = new Text(shell, SWT.BORDER);
        data = new GridData(GridData.FILL_HORIZONTAL);
        data.horizontalSpan = 2;
        text.setLayoutData(data);
        text.addListener(SWT.Verify, new VerifyListener());

        // Show the message
        Label label1 = new Label(shell, SWT.NONE);
        label1.setText("������� ����� ���");
        data = new GridData();
        data.horizontalSpan = 2;
        label1.setLayoutData(data);

        // Display the input box
        final Text text1 = new Text(shell, SWT.BORDER);
        data = new GridData(GridData.FILL_HORIZONTAL);
        data.horizontalSpan = 2;
        text1.setLayoutData(data);
        text1.addListener(SWT.Verify, new VerifyListener());

        // Create the OK button and add a handler
        // so that pressing it will set input
        // to the entered value
        Button ok = new Button(shell, SWT.PUSH);
        ok.setText("OK");
        data = new GridData(GridData.FILL_HORIZONTAL);
        ok.setLayoutData(data);

        ok.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
                isCloseNormal = true;
                libraWeight = text.getText();
                commonWeight = text1.getText();
                shell.close();
            }
        });

        // Create the cancel button and add a handler
        // so that pressing it will set input to null
        Button cancel = new Button(shell, SWT.PUSH);
        cancel.setText("Cancel");
        data = new GridData(GridData.FILL_HORIZONTAL);
        cancel.setLayoutData(data);
        cancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
                isCloseNormal = false;
                shell.close();
            }
        });

        // Set the OK button as the default, so
        // user can type input and press Enter
        // to dismiss
        shell.setDefaultButton(ok);
    }

    static private class VerifyListener implements Listener {
        public void handleEvent(Event e) {
            String string = e.text;
            char[] chars = new char[string.length()];
            string.getChars(0, chars.length, chars, 0);
            for (int i = 0; i < chars.length; i++) {
                if (!('0' <= chars[i] && chars[i] <= '9')) {
                    e.doit = false;
                    return;
                }
            }
        }
    };

}