package org.kamaz.ui;

import org.eclipse.jface.wizard.WizardPage;
import org.kamaz.RecognitionBuilderResult;

/**
 * Encapsulate user business logic before or after load page
 *
 */
public abstract class CommonWizardPage extends WizardPage {
    
    protected RecognitionBuilderResult recognitionBuilderResult;

    protected CommonWizardPage(RecognitionBuilderResult recognitionBuilderResult, String pageName) {
        super(pageName);
        this.recognitionBuilderResult = recognitionBuilderResult;
    }

    protected void beforeLoad() {

    }

}