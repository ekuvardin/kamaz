package org.kamaz.ui.pages;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.kamaz.DetailEntity;
import org.kamaz.ITemplateStore;
import org.kamaz.RecognitionBuilderResult;
import org.kamaz.ui.CommonWizardPage;

public class RecognitionResultPage extends CommonWizardPage {
    private Composite container;
    private Table table;
    private ITemplateStore store;
    TableColumn column1;
    TableColumn column2;

    public RecognitionResultPage(RecognitionBuilderResult recognitionBuilderResult, ITemplateStore store) {
        super(recognitionBuilderResult, "Second Page");
        setTitle("���������� �������� �������������");
        setDescription("����������� ������ � ���������������:");
        this.store = store;
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 1;

        Label label1 = new Label(container, SWT.NONE);
        label1.setText("����� ������ ���� ������");
        
        table = new Table(container, SWT.BORDER);
        column1 = new TableColumn(table, SWT.NONE);
        column2 = new TableColumn(table, SWT.NONE);

        column1.setText("�������������");
        column2.setText("������");

        table.addListener(SWT.Selection, new Listener() {

            @Override
            public void handleEvent(Event e) {
                setPageComplete(true);
                TableItem[] selection = table.getSelection();
                String identifier = selection[0].getText();
                recognitionBuilderResult.setDetailEntity(store.getTemplateByKey(identifier));
                setDescription("����������� ������ � ���������������: " + identifier);
            }

        });

        column1.pack();
        column2.pack();

        Button check = new Button(container, SWT.PUSH);
        check.setText("����� ������ �� ��������������");

        setControl(container);
        setPageComplete(false);
    }

    @Override
    protected void beforeLoad() {
        table.removeAll();
        
        for (DetailEntity detailEntity : store.iterator()) {
            TableItem item = new TableItem(table, SWT.NONE);
            item.setText(0, detailEntity.getIdentifier());
            item.setImage(1, detailEntity.getImage());
        }

        column1.pack();
        column2.pack();
        container.layout();
    }

}