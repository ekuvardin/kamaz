package org.kamaz.ui.pages;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.kamaz.RecognitionBuilderResult;
import org.kamaz.ui.CommonWizardPage;

public class CameraViewPage extends CommonWizardPage {
    private Composite container;

    public CameraViewPage(RecognitionBuilderResult recognitionBuilderResult) {
        super(recognitionBuilderResult, "First Page");
        setTitle("������ ���. ������ ��� �������");
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 1;
        Label label1 = new Label(container, SWT.NONE);
        label1.setText("��������� ������ ��� ������");
        // required to avoid an error in the system
        setControl(container);
        setPageComplete(true);
    }

}