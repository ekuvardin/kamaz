package org.kamaz.ui.pages;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.kamaz.DetailEntity;
import org.kamaz.RecognitionBuilderResult;
import org.kamaz.RecognitionResult;
import org.kamaz.ui.CommonWizardPage;

public class ResultPage extends CommonWizardPage {
    private Composite container;
    private Text text1;
    private Text text2;
    private Text text3;
    private Label label4;

    public ResultPage(RecognitionBuilderResult recognitionBuilderResult) {
        super(recognitionBuilderResult, "Third Page");
        setTitle("�������� ���������");
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;

        Label label1 = new Label(container, SWT.NONE);
        label1.setText("����� ���");

        text1 = new Text(container, SWT.BORDER | SWT.SINGLE);
        text1.setText("3456");
        text1.setEditable(false);

        Label label2 = new Label(container, SWT.NONE);
        label2.setText("������������� ������");

        text2 = new Text(container, SWT.BORDER | SWT.SINGLE);
        text2.setText("");
        text2.setEditable(false);

        Label label3 = new Label(container, SWT.NONE);
        label3.setText("������ ������");

        label4 = new Label(container, SWT.NONE);
        label4.setImage(null);

        Label label5 = new Label(container, SWT.NONE);
        label5.setText("���������� �������");

        text3 = new Text(container, SWT.BORDER | SWT.SINGLE);
        text3.setText("");
        text3.setEditable(false);

        // required to avoid an error in the system
        setControl(container);
        setPageComplete(true);
    }

    @Override
    protected void beforeLoad() {
        RecognitionResult result = recognitionBuilderResult.build();
        DetailEntity detailEntity = result.getDetailEntity();
        text1.setText(String.valueOf(result.getCommonWeight()));
        text2.setText(detailEntity.getIdentifier());
        label4.setImage(detailEntity.getImage());
        text3.setText(String.valueOf((result.getCommonWeight() - result.getLibraWeight()) / detailEntity.getWeight()));

        container.layout();
    }

}