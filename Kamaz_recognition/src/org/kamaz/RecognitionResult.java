package org.kamaz;

/**
 * Final results of working program
 *
 */
public class RecognitionResult {

    private int libraWeight;
    private DetailEntity detailEntity;
    private int commonWeight;
    
    RecognitionResult(int libraWeight, DetailEntity detailEntity, int commonWeight) {
        this.libraWeight = libraWeight;
        this.detailEntity = detailEntity;
        this.commonWeight = commonWeight;
    }

    public int getLibraWeight() {
        return libraWeight;
    }

    public DetailEntity getDetailEntity() {
        return detailEntity;
    }

    public int getCommonWeight() {
        return commonWeight;
    }      

}
