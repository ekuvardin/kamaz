package org.kamaz;

import org.eclipse.swt.graphics.Image;

/**
 *  
 * Describe simple detail
 */
public class DetailEntity {
    
    private String identifier;
    private Image image;
    private int weight;    
    
    public DetailEntity(String identifier, Image image, int weight) {
        this.identifier = identifier;
        this.image = image;
        this.weight = weight;
    }

    public String getIdentifier() {
        return identifier;
    }
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
    public Image getImage() {
        return image;
    }
    public void setImage(Image image) {
        this.image = image;
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }

}
