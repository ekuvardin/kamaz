package org.kamaz;

/**
 * Builder for constructing @RecognitionResult cause @RecognitionResult is building during all phase of working application
 *
 */
public class RecognitionBuilderResult {

    private int libraWeight;
    private DetailEntity detailEntity;
    private int commonWeight;

    public RecognitionBuilderResult setLibraWeight(int libraWeight) {
        this.libraWeight = libraWeight;
        return this;
    }

    public RecognitionBuilderResult setDetailEntity(DetailEntity detailEntity) {
        this.detailEntity = detailEntity;
        return this;
    }
    
    public RecognitionResult build(){
        return new RecognitionResult(this.libraWeight, this.detailEntity, this.commonWeight);
    }

    public void setCommonWeight(int commonWeight) {
        this.commonWeight = commonWeight;
    }

}
