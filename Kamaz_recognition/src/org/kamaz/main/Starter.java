package org.kamaz.main;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.kamaz.DetailEntity;
import org.kamaz.RecognitionBuilderResult;
import org.kamaz.TemplateStore;
import org.kamaz.ui.MainWizard;
import org.kamaz.ui.RecognitionWizardDialog;

/**
 * Main class
 *
 */
public class Starter {

    public static void main(String[] args){
        TemplateStore templateStore = new TemplateStore();
        
        // Load template
        DetailEntity detailEntity1 = new DetailEntity("345ARD343", new Image(Display.getDefault(), "resources/one.png"), 34);
        DetailEntity detailEntity2 = new DetailEntity("111222WWW", new Image(Display.getDefault(), "resources/two.png"), 4);
        templateStore.addValue(detailEntity1);
        templateStore.addValue(detailEntity2);
        
        //Create builder
        RecognitionBuilderResult recognitionBuilderResult = new RecognitionBuilderResult();
        
        //Set weight
        recognitionBuilderResult.setCommonWeight(123456);
        
        // Set libra weight
        recognitionBuilderResult.setLibraWeight(123);
        
        RecognitionWizardDialog dialog = new RecognitionWizardDialog(recognitionBuilderResult, Display.getDefault().getActiveShell(), new MainWizard(recognitionBuilderResult, templateStore));
        dialog.open();
    }
}
