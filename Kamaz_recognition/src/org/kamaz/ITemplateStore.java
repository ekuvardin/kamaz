package org.kamaz;

/**
 * Store details template(icons, identifiers, etc)
 *
 */
public interface ITemplateStore {

    DetailEntity getTemplateByKey(String identifier);
    
    Iterable<DetailEntity> iterator();
    
}
